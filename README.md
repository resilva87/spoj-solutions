# spoj-solutions
Keeping track of solutions for SPOJ (www.spoj.com) problems

## Accepted answers to the following:
**FCTRL**: [http://www.spoj.com/problems/FCTRL/](http://www.spoj.com/problems/FCTRL/)

**INTEST**: [http://www.spoj.com/problems/INTEST/](http://www.spoj.com/problems/INTEST/)

**PRIME1**: [http://www.spoj.com/problems/PRIME1/](http://www.spoj.com/problems/PRIME1/)

**FCTRL2**: [http://www.spoj.com/problems/FCTRL2/](http://www.spoj.com/problems/FCTRL2/)

**TEST**: [http://www.spoj.com/problems/TEST/](http://www.spoj.com/problems/TEST/)

**PROBTNPO**: [http://www.spoj.com/problems/PROBTNPO/](http://www.spoj.com/problems/PROBTNPO/)

**CRYPTO1**: [http://www.spoj.com/problems/CRYPTO1/](http://www.spoj.com/problems/CRYPTO1/)
- Resources
  - [https://exploringnumbertheory.wordpress.com/2013/10/15/solving-quadratic-congruences/](https://exploringnumbertheory.wordpress.com/2013/10/15/solving-quadratic-congruences/)
  - [https://exploringnumbertheory.wordpress.com/2013/08/04/congruence-arithmetic-and-fast-powering-algorithm/](https://exploringnumbertheory.wordpress.com/2013/08/04/congruence-arithmetic-and-fast-powering-algorithm/)
  - [http://deadendmath.com/binary-expansion/](http://deadendmath.com/binary-expansion/)
  - [https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm](https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm)
  - [https://en.wikipedia.org/wiki/Modular_exponentiation](https://en.wikipedia.org/wiki/Modular_exponentiation)

- Solution: Modular exponentiation :bowtie:

**JULKA**: [http://www.spoj.com/problems/JULKA/](http://www.spoj.com/problems/JULKA/)

## In Progress
**BSHEEP**: [http://www.spoj.com/problems/BSHEEP/](http://www.spoj.com/problems/BSHEEP/)
- Resources
  - [https://en.wikipedia.org/wiki/Convex_hull](https://en.wikipedia.org/wiki/Convex_hull)
  - [https://en.wikipedia.org/wiki/Convex_hull_algorithms](https://en.wikipedia.org/wiki/Convex_hull_algorithms)
  - [https://en.wikipedia.org/wiki/Graham_scan](https://en.wikipedia.org/wiki/Graham_scan)
  - [https://en.wikipedia.org/wiki/Polar_coordinate_system](https://en.wikipedia.org/wiki/Polar_coordinate_system)
  - [http://www.cs.princeton.edu/courses/archive/spring03/cs226/lectures/geo.4up.pdf](http://www.cs.princeton.edu/courses/archive/spring03/cs226/lectures/geo.4up.pdf)
  - [http://alienryderflex.com/polygon/](http://alienryderflex.com/polygon/)

- On fast stdin reading in Golang
  - [https://groups.google.com/forum/#!topic/golang-nuts/W08rFBcHKbc](https://groups.google.com/forum/#!topic/golang-nuts/W08rFBcHKbc)
  - [http://play.golang.org/p/1qndGdxdtb](http://play.golang.org/p/1qndGdxdtb)

#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 11*1024

typedef unsigned long long ull;

void fctrl(ull number){

	ull rs = 0;
	
	while(number >= 5){
		rs += number/5;
		number/=5;
	}
	
	printf("%llu\n", rs);
}


int main(){
	
	int chars_read, tests = 0;
	char buf[BUFSIZE+1] = {0}; //initialise all elements to 0
	ull number = 0;
	unsigned long long x = 100;
	
	scanf("%d\n", &tests);
	
	while((chars_read = fread(buf, 1, BUFSIZE, stdin)) > 0){
		  int i = 0;
		  //read the chars from buf
		  for(; i < chars_read; i++){
			//parse the number using characters
			//each number is on a separate line
			if(buf[i] != '\n'){
			  number = buf[i] - '0' + 10*number;
			 }	
			 else{
			  fctrl(number);
			  number = 0;
			  //break;
		  	}
		}
	}
	return 0;
}

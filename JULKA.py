import sys

is_together = True
together = 0
more_apples = 0

while True:
	line = sys.stdin.readline()
	if not line:
		break
	if is_together: 
		together = long(line)
		is_together = False
	else:
		more_apples = long(line)
		natalia = ((together-more_apples)/2)
		klaudia = natalia + more_apples
		print "%d\n%d" % (klaudia, natalia)
		is_together = True

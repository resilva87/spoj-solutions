# https://en.wikipedia.org/wiki/Modular_exponentiation
def power_mod(b, e, m):
    result = 1
    b = b % m
    while e > 0:
        if e & 1 == 1: result = (result * b) % m
        e >>= 1
        b = (b*b) % m
    return result%m

if __name__ == '__main__':
    import time, sys
    # prime number defined in the problem
    m = 4000000007L
    e = 1000000002L
    boundary = 1924991999L

    for line in sys.stdin:
        b = long(line)
        r = power_mod(b,e,m)
        s = m-r
        x = r
        if x > boundary:
            x = s
        print time.strftime("%a %b %d %H:%M:%S %Y", time.gmtime(x))
        sys.exit(0)

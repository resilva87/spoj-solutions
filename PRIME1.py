# Segmented sieve
from math import sqrt, floor
from timeit import timeit as t
import sys

global GREATESTDIFF

GREATESTDIFF = 100000

def seg_sieve(N,M):
	diff = N - M
	primes = [True] * GREATESTDIFF
	primesSieve = []
	FN = int(floor(sqrt(N)))
	k = int(floor(sqrt(FN)))
	
	possiblePrimes = [True] * (diff+1)
	
	for i in xrange(2, k+1):
		if primes[i]:
			for j in xrange(i * i, FN+1, i):
				primes[j] = False
			
	for i in xrange(2,FN+1):
		if primes[i]:
			primesSieve.append(i)

	for prime in primesSieve:
		s = M/prime
		s = s * prime
		for j in xrange(s, N+1, prime):
			if j < M: continue
			possiblePrimes[j-M] = False
				
	r = []	
		
	for i in primesSieve:  		
		if (i >= M and i <= N): r.append(i) 		

	for i in xrange(0, diff+1):  		
		if possiblePrimes[i]: r.append(i+M)

	if 1 in r: r.remove(1)	

	for i in r: print i
	print "\n"

if __name__ == '__main__':
	skip=True
	for line in sys.stdin:
		if skip:
			skip = False
			continue
		else:
			n, m = map(lambda x: int(x), line.strip().split())
			if n < m: 
				n, m = m, n
			seg_sieve(n, m)

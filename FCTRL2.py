def fast_fact(i):
	if i >= 0 and i <= 2:
		print "%s" % i
	else:
		stop = 2 if i % 2 == 0 else 3
		j = i
		acc = 0
		u = 1
		r = 1
		while j >= stop:	
			acc = acc + j
			r = r * acc
			j = (i - u * 2)
			u = u + 1
			
		if stop == 3: r = r * ((i/2)+1)	
		
		print "%s" % r	
	
if __name__ == '__main__':
	import sys
	skip = True
	for l in sys.stdin:
		if skip:
			skip = False
		else:
			x = int(l.strip())
			fast_fact(x)

import math, numbers

global cache
cache = {}

def g(x):
	if x == 1: return 1
	elif x in cache.keys():
		return cache[x]
	elif(x % 2 == 0):
		cache[x] = 1 + g(x/2)
	else:
		cache[x] = 1 + g(3 * x + 1)
	return cache[x]
			
def main(i, j):
	a = 0
	for x in range(i, j+1):
		u = g(x)
		if u > a: a = u
	return a
	
if __name__ == '__main__':
	import sys
	for line in sys.stdin:
		x = line.strip().split()
		i = int(x[0])
		j = int(x[1])
		x = i
		y = j
		if i > j:
			i, j = j, i
		print("%s %s %s" % (x,y,int(main(i, j))))

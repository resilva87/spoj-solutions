//https://en.wikipedia.org/wiki/Graham_scan
package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

// const GOD_MODE bool = false

/**
* Basic types
**/
const CLOCKWISE int = -1
const COUNTERCLOCKWISE int = 1
const COLINEAR int = 0

// Type Point represents a Point in 2D-space, identified by an int
type Point struct {
	X   int
	Y   int
	Idx int
}

// toString
func (p *Point) String() string {
	return fmt.Sprintf("Point %d {X:%d, Y:%d}", p.Idx, p.X, p.Y)
}

// Equals
func (p *Point) Equals(other *Point) bool {
	return p.X == other.X && p.Y == other.Y
}

// Calculate the (squared) distance of this point to another
func (p *Point) SquaredDistance(other *Point) float64 {
	dx := (p.X - other.X) * (p.X - other.X) // aka squared x distance
	dy := (p.Y - other.Y) * (p.Y - other.Y) // aka squared y distance
	return float64(dx + dy)
}

// Calculate the distance of this point to another
func (p *Point) Distance(other *Point) float64 {
	return math.Sqrt(p.SquaredDistance(other))
}

// Type PointColl represents a collection of Point objects
type PointColl []*Point

// Implementation of the sort.Interface (https://golang.org/pkg/sort/#Interface)
func (p PointColl) Len() int      { return len(p) }
func (p PointColl) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

// Helper function to check if point exists in the collection
// The collection is not sorted yet, so we have to use a linear search
func (p PointColl) Exists(point *Point) bool {
	for _, x := range p {
		if point.Equals(x) {
			return true
		}
	}
	return false
}

// Less function is a bit more customised. Instead of simply comparing the points by X,Y coordinates,
// it follows comparation as defined by Graham's Scan method
// Sorts p[1:] points in the collection by polar angle in counterclockwise order around p[0]
func (p PointColl) Less(i, j int) bool {
	start := p[0] // aka lowest point
	p1 := p[i]
	p2 := p[j]
	o := orientation(start, p1, p2)
	if o == COUNTERCLOCKWISE {
		return true
	} else if o == COLINEAR {
		// if points start, p1 and p2 are colinear, them we must compare their distances to start point
		d1 := start.SquaredDistance(p1)
		d2 := start.SquaredDistance(p2)
		if d2 >= d1 {
			return true
		}
	}
	return false
}

func (p PointColl) Print() {
	for _, v := range p {
		fmt.Printf("%s\n", v)
	}
}

func (p PointColl) PrintIndex() {
	for i, v := range p {
		fmt.Printf("%d", v.Idx)
		if i+1 < len(p) {
			fmt.Printf(" ")
		}
	}
}

// FindBottomMostPoint returns the lowest Y, X point in the collection
func (p PointColl) FindBottomMostPoint() *Point {
	lowest := p[0]
	for _, v := range p {
		if v.Y < lowest.Y || (v.Y == lowest.Y && v.X < lowest.X) {
			lowest = v
		}
	}
	return lowest
}

// Type Stack (https://gist.github.com/bemasher/1777766)
type Stack struct {
	top  *Element
	size int
}

type Element struct {
	value interface{} // All types satisfy the empty interface, so we can store anything here.
	next  *Element
}

// Return the stack's length
func (s *Stack) Len() int {
	return s.size
}

// Return if the stack's empty
func (s *Stack) IsEmpty() bool {
	return s == nil || s.size == 0
}

// Push a new element onto the stack
func (s *Stack) Push(value interface{}) {
	s.top = &Element{value, s.top}
	s.size++
}

// Remove the top element from the stack and return it's value
// If the stack is empty, return nil
func (s *Stack) Pop() (value interface{}) {
	if !s.IsEmpty() {
		value, s.top = s.top.value, s.top.next
		s.size--
		return
	}
	return nil
}

//Peek returns a top without removing it from list
func (s *Stack) Top() (value interface{}) {
	if !s.IsEmpty() {
		value = s.top.value
	}
	return
}

// Return the closest element to the top of the stack
// If the stack size is <= 1, return nil
func (s *Stack) NextToTop() (value interface{}) {
	if s.size > 1 {
		t := s.Pop()
		r := s.Pop()
		s.Push(r)
		s.Push(t)
		return r
	}
	return nil
}

func (s *Stack) Traverse(fn func(interface{})) {
	for t := s.top; t != nil; t = t.next {
			fn(t.value)
		}
}

func (s *Stack) Print() {
	s.Traverse(func(v interface{}){ fmt.Printf("%v\n", v)})
}

/**
* Auxiliary functions
**/
// Parses (x y) string to a Point object
func parsePosition(pos string) *Point {
	k := strings.Split(pos, " ")
	x, _ := strconv.Atoi(k[0])
	y, _ := strconv.Atoi(k[1])
	return &Point{x, y, 0}
}

// Helper function to find the orientation of 3 points (x, y, z)
/**
# Three points are a counter-clockwise turn if ccw > 0, clockwise if
# ccw < 0, and collinear if ccw = 0 because ccw is a determinant that
# gives twice the signed  area of the triangle formed by p1, p2 and p3.
**/
func orientation(p, q, r *Point) int {
	d := (q.Y-p.Y)*(r.X-q.X) - (q.X-p.X)*(r.Y-q.Y)
	if d > 0 {
		return CLOCKWISE
	} else if d < 0 {
		return COUNTERCLOCKWISE
	}
	return COLINEAR
}

/**
 * http://play.golang.org/p/1qndGdxdtb
 **/
// atoi parses a buffer into an integer
// without overflow detection.
func atoi(b []byte) (int, error) {
	neg := false
	if b[0] == '+' {
		b = b[1:]
	} else if b[0] == '-' {
		neg = true
		b = b[1:]
	}
	n := 0
	for _, v := range b {
		if v < '0' || v > '9' {
			return 0, strconv.ErrSyntax
		}
		n = n*10 + int(v-'0')
	}
	if neg {
		return -n, nil
	}
	return n, nil
}

func readInt(scanner *bufio.Scanner) (int, error) {
	for scanner.Scan() {
		bytes := scanner.Bytes()
		if bytes == nil || len(bytes) == 0 {
			continue
		}
		return atoi(bytes)
	}
	return 0, strconv.ErrSyntax
}

func pointInPolygon(x, y int, xs, ys []int) bool {
	if xs == nil || ys == nil || len(xs) == 0 || len(ys) == 0 {
		return false
	} else if len(xs) < 4 || len(ys) < 4 {
		return false
	}
	c := len(xs)

	// http://www.realtimerendering.com/resources/GraphicsGems//gemsiv/ptpoly_haines/ptinpoly.c
	// vtx0x := xs[c-1]
	// vtx0y := ys[c-1]
	// vtx1x := xs[0]
	// vtx1y := ys[0]
	// yflag0 := (vtx0y >= y)
	//
	// inside := false
	//
	// for i, j := 1, c-1; j > 0; {
	// 	yflag1 := (vtx1y >= y)
	// 	if yflag1 != yflag0 {
	// 		if ( ((vtx1y-y) * (vtx0x-vtx1x) >= (vtx1x-x) * (vtx0y-vtx1y)) == yflag1 ) {
	// 				inside = !inside
	//     }
	// 	}
	// 	yflag0 = yflag1
	// 	vtx0x = vtx1x
	// 	vtx0y = vtx1y
	// 	vtx1x = xs[i]
	// 	vtx1y = ys[i]
	// 	j--
	// 	i++
	// }
	//
	// return inside

	res := false

	for i, j := 0, c-1; i < c; i++ {
		if ((ys[i] > y) != (ys[j] > y) && (x < (xs[j] - xs[i]) * (y - ys[i]) / (ys[j]-ys[i]) + xs[i])) {
          res = !res;
    }
		j = i
	}

	return res
}

/**
 * Main function - GrahamScan
 **/
func GrahamScan(points PointColl, n int) (PointColl, float64) {
	s := new(Stack)
	// colinear := new(Stack)
	s.Push(points[1])
	s.Push(points[2])

	for i := 2; i <= n; i++ {
		// notFoundColinear := false
		for {
			nt, k := s.NextToTop().(*Point)
			if !k {
				break
			}
			t := s.Top().(*Point)
			p := points[i]
			o := orientation(nt, t, p)
			// if GOD_MODE {
			// 	fmt.Printf("Top: %s\n", t)
			// 	fmt.Printf("Next to Top: %s\n", nt)
			// 	fmt.Printf("Point[%d]: %s\n", i, p)
			// 	fmt.Printf("Resulting orientation: %d\n", o)
			// }
			if o == COUNTERCLOCKWISE {
				// notFoundColinear = true
				break
			}
			_, ok := s.Pop().(*Point)
			if !ok {
				break
			}
		}
		// if !notFoundColinear {
		// 	if GOD_MODE {
		// 		fmt.Printf("Adding colinear point: %s\n", points[i])
		// 	}
		// 	colinear.Push(points[i])
		// }
		s.Push(points[i])
		// if GOD_MODE {
		// 	fmt.Println("Stack:")
		// 	s.Print()
		// 	fmt.Println("")
		// }
	}

	var result PointColl
	// The remaining elements in S belongs to the convex hull
	// As the last element in the stack is a copy of the bottom most point,
	// we discard it.
	for i := s.Len() - 1; i >= 0; i-- {
		p := s.Pop().(*Point)
		result = append(PointColl{p}, result...)
	}

	var acc float64
	for i := 1 ; i < result.Len(); i++ {
		acc += result[i].Distance(result[i-1])
	}
	acc += result[result.Len()-1].Distance(result[0])
	return result, acc
}

func HandleTwoPoints(points PointColl, n int) {
	b := points[1]
	t := points[n]
	d := 2 * b.Distance(t)
	fmt.Printf("%.2f\n", d)
	fmt.Printf("%d %d", b.Idx, t.Idx)
	fmt.Println("\n")
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	nrOfTests, _ := readInt(scanner)

	for i := 0; i < nrOfTests; i++ {
		n, _ := readInt(scanner)
		var points PointColl
		var xs, ys []int
		duplicates := 0
		for j := 0; j < n; j++ {
			//pos, _ := reader.ReadString('\n')
			//point := parsePosition(strings.TrimSpace(pos))
			point := new(Point)
			point.X, _ = readInt(scanner)
			point.Y, _ = readInt(scanner)
			point.Idx = j + 1
			inside := pointInPolygon(point.X, point.Y, xs, ys)
			if points.Exists(point) || inside {
				duplicates++
			} else {
				points = append(points, point)
				xs = append(xs, point.X)
				ys = append(ys, point.Y)
			}
		}

		n -= duplicates

		if n == 1 {
			fmt.Println("0.0")
			fmt.Println("1")
			fmt.Println("")
			continue
		}

		lowest := points.FindBottomMostPoint()
		points = append(PointColl{lowest}, points...)
		sort.Sort(points)

		if n == 2 {
			HandleTwoPoints(points, n)
			continue
		}

		ch, d := GrahamScan(points, n)

		// if ch == nil {
		// 	HandleTwoPoints(points, n)
		// 	continue
		// }

		fmt.Printf("%.2f\n", d)
		ch.PrintIndex()
		fmt.Println("\n")
	}
}
